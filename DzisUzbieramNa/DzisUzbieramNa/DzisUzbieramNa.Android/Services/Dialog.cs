﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


[assembly: Xamarin.Forms.Dependency(typeof(DzisUzbieramNa.Droid.Services.Dialog))]
namespace DzisUzbieramNa.Droid.Services
{
    class Dialog : DzisUzbieramNa.Interface.IDialog
    {
        public void ShowDialog(string message)
        {
            MainActivity.ShowToast(message);
        }
    }
}