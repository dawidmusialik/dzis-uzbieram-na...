﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DzisUzbieramNa.DataBase.Table
{
    [Table("AmountCollectedTable")]
    public class AmountCollectedTable
    {
        [PrimaryKey, AutoIncrement, Column("id")]
        public int id { get; set; }
        public int idItem { get; set; }
        public double amount { get; set; }
        public DateTime date { get; set; }
    }
}
