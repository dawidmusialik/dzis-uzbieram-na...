﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DzisUzbieramNa.DataBase.Table
{
    [Table("CategoryTable")]
    public class CategoryTable
    {
        [PrimaryKey, AutoIncrement, Column("id")]
        public int id { get; set; }

        public string category { get; set; }
    }
}
