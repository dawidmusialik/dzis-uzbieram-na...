﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DzisUzbieramNa.DataBase.Table
{
    [Table("DesiredItemTable")]
    public class DesiredItemTable
    {
        [PrimaryKey, AutoIncrement, Column("id")]
        public int id { get; set; }

        public string imagePath { get; set; }
        public string title { get; set; }
        public string description { get; set; }

        public double price { get; set; }
        public double amountCollected { get; set; }

        public int idCategory { get; set; }

        public bool bought { get; set; }

        public DateTime dateWhenToBuyIt { get; set; }
        public DateTime dateWhenWasBought { get; set; }
    }
}
