﻿using DzisUzbieramNa.DataBase.Table;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DzisUzbieramNa.DataBase.Command
{
    /// <summary>
    /// Klasa odpowiedzialna za komendy dotyczące tabeli AmmountCollected -> czyli lista wpłaconych/wypłaconych oszczędności
    /// </summary>
    public class AmountCollectedCommand
    {
        /// <value> Zmienna <c>db</c> odpowiadająca za połączenie z bazą danych</value>
        private SQLiteConnection db;

        /// <summary>
        /// Kontruktor domyślny dla klasy AmountCollectedCommand
        /// </summary>
        public AmountCollectedCommand()
        {
            db = DataBase.Connection.Instance.getDataBaseConnection();
            CreateTable();

        }
        /// <summary>
        /// Metoda prywatna odpowiadająca za utworzenie tabeli <c>AmountCollectedTable</c>, wykorzystuje ona metodę <c>addDefault</c>
        /// </summary>
        private void CreateTable()
        {
            try
            {
                ///<value>Zmienna <c>TableAmountCollected</c> dzięki, której metoda sprawdza czy tabela została już utworzona</value>
                var TableAmountCollected = db.GetTableInfo("AmountCollectedTable");

                if (TableAmountCollected.Count == 0)
                {
                    db.CreateTable<AmountCollectedTable>();
                    addDefault();
                    Console.WriteLine("Tabela AmountCollectedTable została utworzona");
                }
                else
                {
                    Console.WriteLine("Tabela AmountCollectedTable została już utworzona");
                }
            }

            catch (Exception ex)
            {
            }
        }


        /// <summary>
        /// Metoda prywatna dodająca defaultowe elementy do tabeli, wykorzystuje metodę <c>_helpDefaultAmoundAndDesiredItem</c>
        /// </summary>
        private void addDefault()
        {
            _helpDefaultAmoundAndDesiredItem(350, new DateTime(2018, 7, 18, 0, 0, 0, DateTimeKind.Local), 1, db, "Dodawanie");
            _helpDefaultAmoundAndDesiredItem(55, new DateTime(2018, 7, 18, 0, 0, 0, DateTimeKind.Local), 1, db, "Odejmowanie");
            _helpDefaultAmoundAndDesiredItem(400, new DateTime(2018, 7, 19, 0, 0, 0, DateTimeKind.Local), 1, db, "Dodawanie");
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_amount">Zmienna odpowiadająca za wielkość <c>oszczędności</c></param>
        /// <param name="_date"><c>Data</c> dodania lub odjęcia oszczędności</param>
        /// <param name="_idItem"></param>
        /// <param name="db"></param>
        /// <param name="operation"></param>
        private void _helpDefaultAmoundAndDesiredItem(double _amount, DateTime _date, int _idItem, SQLiteConnection db, string operation)
        {
            AmountCollectedTable _amountCollected;
            DesiredItemTable _desiredItem;

            _amountCollected = new AmountCollectedTable();
            _desiredItem = db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _idItem).FirstOrDefault();

            if (operation == "Dodawanie")
            {
                _amountCollected.amount = _amount;
                _amountCollected.date = _date;
                _amountCollected.idItem = _idItem;

                _desiredItem.amountCollected += _amount;
            }
            if (operation == "Odejmowanie")
            {
                _amountCollected.amount = -_amount;
                _amountCollected.date = DateTime.SpecifyKind(_date, DateTimeKind.Local);
                _amountCollected.idItem = _idItem;

                if (_desiredItem.amountCollected > _amount)
                {
                    _desiredItem.amountCollected -= _amount;
                }
            }

            db.Insert(_amountCollected);
            db.UpdateWithChildren(_desiredItem);
        }

        public bool addAmountCollected_Addition(double _amount, DateTime _date, int _idItem)
        {
            try
            {
                var _amountCollected = new AmountCollectedTable();
                _amountCollected.amount = _amount;
                _amountCollected.date = DateTime.SpecifyKind(_date, DateTimeKind.Local); ;
                _amountCollected.idItem = _idItem;

                var _desiredItem = db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _idItem).FirstOrDefault();
                _desiredItem.amountCollected += _amount;


                db.Insert(_amountCollected);
                db.UpdateWithChildren(_desiredItem);

                App.LocalNotification("Dodano oszczędności");
                return true;
            }
            catch
            {
                return false;
            }
            
        }
        public bool addAmountCollected_Subtraction(double _amount, DateTime _date, int _idItem)
        {
            try
            {
                var _amountCollected = new AmountCollectedTable();
                _amountCollected.amount = -_amount;
                _amountCollected.date = DateTime.SpecifyKind(_date, DateTimeKind.Local);
                _amountCollected.idItem = _idItem;

                var _desiredItem = db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _idItem).FirstOrDefault();

                if(_desiredItem.amountCollected > _amount)
                {
                    _desiredItem.amountCollected -= _amount;
                }
                else
                {
                    App.LocalNotification("Zbyt mało oszczędności by odjąć");
                    return false;
                }

                db.Insert(_amountCollected);
                db.UpdateWithChildren(_desiredItem);

                App.LocalNotification("Odjęto oszczędności");

                return true;
            }
            catch
            {
                return false;
            }
            
        }

        public List<Model.AmountCollectedModel> getAllAmountCollected(int _idItem)
        {
            List<Model.AmountCollectedModel> _amountCollected = new List<Model.AmountCollectedModel>();
            var _allAmount = db.GetAllWithChildren<AmountCollectedTable>().Where(x => x.idItem == _idItem);

            
            foreach(var item in _allAmount)
            {
                Model.AmountCollectedModel _localAmountCollected = new Model.AmountCollectedModel();
                _localAmountCollected.amount = Convert.ToString(item.amount);
                _localAmountCollected.date = item.date.Date.ToLocalTime().ToShortDateString();
                _localAmountCollected.idItem = item.idItem;
                _localAmountCollected.id = item.id;

                _amountCollected.Add(_localAmountCollected);
            }

            return _amountCollected;
        }
        

        public void deleteAmountCollected(int _idAmountCollected)
        {
            var _amountCollected = db.GetAllWithChildren<AmountCollectedTable>().Where(x => x.id == _idAmountCollected).FirstOrDefault();
            var _desiredItem = db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _amountCollected.idItem).FirstOrDefault();

            if(_amountCollected.amount < 0)
            {
                _desiredItem.amountCollected += _amountCollected.amount;
            }
            else
            {
                _desiredItem.amountCollected -= _amountCollected.amount;
            }

            db.Delete(_amountCollected);
            db.UpdateWithChildren(_desiredItem);
        }
        public void deleteAllAmountCollected(int _idDesiredItem)
        {
            var _amountCollected = db.GetAllWithChildren<AmountCollectedTable>().Where(x => x.idItem == _idDesiredItem);
            if(_amountCollected != null)
            {
                if (_amountCollected.ToList().Count != 0)
                {
                    foreach (var item in _amountCollected)
                    {
                        db.Delete(item);

                    }
                }
            }
            
        }

        public bool checkDesiredItemHasAmountCollected(int _idDesiredItem)
        {
            var _amountCollected = db.GetAllWithChildren<AmountCollectedTable>().Where(x => x.idItem == _idDesiredItem);
            if (_amountCollected != null)
            {
                if (_amountCollected.ToList().Count != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public void updateAmountCollected(int _idAmountCollected, double _amount, DateTime _date)
        {
            var _amountCollected = db.GetAllWithChildren<AmountCollectedTable>().Where(x => x.id == _idAmountCollected).FirstOrDefault();
            var _desiredItem = db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _amountCollected.idItem).FirstOrDefault();

            if (_amountCollected.amount < 0)
            {
                _desiredItem.amountCollected += _amountCollected.amount;
            }
            else
            {
                _desiredItem.amountCollected -= _amountCollected.amount;
            }

            _amountCollected.amount = _amount;
            _amountCollected.date = _date;

            if (_amountCollected.amount < 0)
            {
                _desiredItem.amountCollected -= _amountCollected.amount;
            }
            else
            {
                _desiredItem.amountCollected += _amountCollected.amount;
            }

            db.UpdateWithChildren(_amountCollected);
            db.UpdateWithChildren(_desiredItem);

        }

    }
}
