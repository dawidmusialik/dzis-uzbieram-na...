﻿
using Android.Content.Res;
using Android.Graphics.Drawables;
using DzisUzbieramNa.DataBase.Table;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.DataBase.Command
{
    public class DesiredItemCommand
    {
        private SQLiteConnection db;

        public DesiredItemCommand()
        {
            db = DataBase.Connection.Instance.getDataBaseConnection();
            CreateTable();

        }
        private void CreateTable()
        {
            try
            {
                var TableDesiredItem = db.GetTableInfo("DesiredItemTable");

                if (TableDesiredItem.Count == 0)
                {
                    db.CreateTable<DesiredItemTable>();
                    createDefaultItem();
                    Console.WriteLine("Tabela TableDesiredItem została utworzona");
                }
                else
                {
                    Console.WriteLine("Tabela TableDesiredItem została już utworzona");
                }
            }

            catch (Exception ex)
            {
            }
        }

        private void createDefaultItem()
        {
            DesiredItemTable _objectMonitorPredator = new DesiredItemTable();
            _objectMonitorPredator.bought = false;
            _objectMonitorPredator.imagePath = "imageAcerPredator.jpg";
            _objectMonitorPredator.amountCollected = 0;
            _objectMonitorPredator.price = 1199;
            _objectMonitorPredator.dateWhenToBuyIt = new DateTime(2018, 7, 17, 0, 0, 0, DateTimeKind.Local);
            _objectMonitorPredator.description = "Acer Predator XB240HBBMJDPR \n Przekątna: 24\" Rozdzielczość: 1920 x 1080 Matryca: (LED, TN) Powłoka matrycy: Matowa";
            _objectMonitorPredator.title = "Nowy Monitor";
            _objectMonitorPredator.idCategory = 2;
            db.Insert(_objectMonitorPredator);

        }


        public void addDesiredItem(string _imagePath, string _title, string _description, double _price, int _idCategory, DateTime _dateWhenToBuyIt)
        {
            var _desiredItem = new DesiredItemTable();
            _desiredItem.imagePath = _imagePath;
            _desiredItem.title = _title;
            _desiredItem.description = _description;
            _desiredItem.price = _price;
            _desiredItem.idCategory = _idCategory;
            _desiredItem.dateWhenToBuyIt = _dateWhenToBuyIt;

            _desiredItem.bought = false;
            db.Insert(_desiredItem);

        }
        public bool addDesiredItem(DesiredItemTable _desiredItem)
        {
            try
            {
                db.Insert(_desiredItem);
                App.LocalNotification("Dodano nowy przedmiot");
                return true;
            }
            catch
            {
                App.LocalNotification("Wystąpił nioczekiwany błąd podczas dodawania nowego przedmiotu");
                return false;
            }
        }

        public bool deleteDesiredItem(int _idItem)
        {
            try
            {
                if (db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _idItem) != null)
                {
                    if(db.GetAllWithChildren<DesiredItemTable>().Count > 0)
                    {
                        App.dbAmountCollected.deleteAllAmountCollected(_idItem);
                        if(File.Exists(db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _idItem).Select(x=>x.imagePath).FirstOrDefault()) == true)
                        {
                            File.Delete(db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _idItem).Select(x => x.imagePath).FirstOrDefault());
                        }
                        db.Delete<DesiredItemTable>(_idItem);

                        App.LocalNotification("Usunięto przedmiot");
                        return true;
                    }
                    else
                    {
                        App.LocalNotification("Brak przedmiotu do usunięcia");
                        return false;
                    }
                }
                else
                {
                    App.LocalNotification("Nie udało się znaleźć przedmiotu do usunięcia");
                    return false;
                }
            }
            catch
            {
                App.LocalNotification("Wystąpił nioczekiwany błąd podczas usuwania przedmiotu");
                return false;
            }

        }

        public List<Model.DesiredItemModelShort> getAllDesiredItemShort()
        {
           var _desiredItemDB = db.GetAllWithChildren<DesiredItemTable>();
           var _desiredItem = new List<Model.DesiredItemModelShort>();


           foreach(var item in _desiredItemDB)
            {
                Model.DesiredItemModelShort _localDesiredItem = new Model.DesiredItemModelShort();

                _localDesiredItem.id = item.id;
                _localDesiredItem.price = Convert.ToString(item.price);
                _localDesiredItem.title = item.title;
                _localDesiredItem.description = item.description;
                _localDesiredItem.category = App.dbCategory.getStringCategory(item.idCategory);
                _localDesiredItem.amountCollected = Convert.ToString(item.amountCollected);
                
                if(item.bought == true)
                {
                    _localDesiredItem.isBought = Color.Green;
                }
                else
                {
                    _localDesiredItem.isBought = Color.White;
                }

                if (item.imagePath != null && item.imagePath != "")
                {
                    //if(File.Exists(item.imagePath) == true)
                    //{
                    _localDesiredItem.imagePath = item.imagePath;
                    //}
                    //else
                    //{
                    //_localDesiredItem.imagePath = "iconNoImage.png";
                    //}

                }
                else
                {
                    _localDesiredItem.imagePath = "iconNoImage.png" ;
                }

                _desiredItem.Add(_localDesiredItem);
            }
            return _desiredItem;
        }

        public List<Model.DesiredItemModelLong> getAllDesiredItemLong()
        {
            var _desiredItemDB = db.GetAllWithChildren<DesiredItemTable>();
            var _desiredItemLong = new List<Model.DesiredItemModelLong>();

            foreach (var item in _desiredItemDB)
            {
                Model.DesiredItemModelLong _localDesireItem = new Model.DesiredItemModelLong();
                _localDesireItem.id = item.id;
                _localDesireItem.amountCollected = Convert.ToString(item.amountCollected);
                if(item.bought == true)
                {
                    _localDesireItem.bought = "Tak";
                    _localDesireItem.isBought = Color.Green;

                }
                else
                {
                    _localDesireItem.bought = "Nie";
                    _localDesireItem.isBought = Color.White;

                }
                _localDesireItem.category = App.dbCategory.getStringCategory(item.idCategory);
                _localDesireItem.dateWhenToBuyIt = item.dateWhenToBuyIt;
                _localDesireItem.dateWhenWasBought = item.dateWhenWasBought;
                _localDesireItem.description = item.description;
                _localDesireItem.price = Convert.ToString(item.price);
                _localDesireItem.title = item.title;

                if (item.imagePath != null && item.imagePath != "")
                {
                    //if (File.Exists(item.imagePath) == true)
                    //{
                        _localDesireItem.imagePath = item.imagePath;
                    //}
                    //else
                    //{
                    //    _localDesireItem.imagePath = "iconNoImage.png";
                    //}
                }
                else
                {
                    _localDesireItem.imagePath = "iconNoImage.png";
                }

                _desiredItemLong.Add(_localDesireItem);
            }

                return _desiredItemLong;
        }
        public Model.DesiredItemModelLong getDesiredItemLong(int _idDesiredItem)
        {
            var _desiredItemDB = db.GetAllWithChildren<DesiredItemTable>().Where(x=>x.id == _idDesiredItem).FirstOrDefault();
            
                Model.DesiredItemModelLong _localDesireItem = new Model.DesiredItemModelLong();

                _localDesireItem.id = _desiredItemDB.id;
                _localDesireItem.amountCollected = Convert.ToString(_desiredItemDB.amountCollected);
                if (_desiredItemDB.bought == true)
                {
                    _localDesireItem.bought = "Tak";
                    _localDesireItem.isBought = Color.Green;

                }
            else
                {
                    _localDesireItem.bought = "Nie";
                    _localDesireItem.isBought = Color.White;

                }
                _localDesireItem.category = App.dbCategory.getStringCategory(_desiredItemDB.idCategory);
                _localDesireItem.dateWhenToBuyIt = _desiredItemDB.dateWhenToBuyIt.ToLocalTime();
                _localDesireItem.dateWhenWasBought = _desiredItemDB.dateWhenWasBought.ToLocalTime();
                _localDesireItem.description = _desiredItemDB.description;
                _localDesireItem.price = Convert.ToString(_desiredItemDB.price);
                _localDesireItem.title = _desiredItemDB.title;

                if (_desiredItemDB.imagePath != null && _desiredItemDB.imagePath != "")
                {
                    //if (File.Exists(_desiredItemDB.imagePath) == true)
                    //{
                        _localDesireItem.imagePath = _desiredItemDB.imagePath;
                    //}
                    //else
                    //{
                    //    _localDesireItem.imagePath = "iconNoImage.png";
                    //}
                }
                else
                {
                    _localDesireItem.imagePath = "iconNoImage.png";
                }


            return _localDesireItem;

        }

        public string getDesiredItemTitle(int _idItem)
        {
            var _desiredItemDB = db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _idItem).Select(x=>x.title).FirstOrDefault();
            return _desiredItemDB;
        }

        public string getDesiredItemImagePath( int _idItem)
        {
            var _desiredItemDB = db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _idItem).Select(x => x.imagePath).FirstOrDefault();
            return _desiredItemDB;
        }
        public double getDesiredItemAmountCollected(int _idDesiredItem)
        {
            var _desiredItemDB = db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _idDesiredItem).Select(x => x.amountCollected).FirstOrDefault();
            return _desiredItemDB;
        }

        public bool updateDesiredItem(Model.DesiredItemModelLong _desiredItem, int _idItem)
        {
            try
            {
                var _localDesiredItem = db.GetAllWithChildren<DesiredItemTable>().Where(x => x.id == _idItem).FirstOrDefault();

                if (File.Exists(App.dbDesiredItem.getDesiredItemImagePath(_idItem)) == true)
                {
                    File.Delete(App.dbDesiredItem.getDesiredItemImagePath(_idItem));
                }

                _localDesiredItem.imagePath = _desiredItem.imagePath;
                _localDesiredItem.description = _desiredItem.description;
                _localDesiredItem.price = Convert.ToDouble(_desiredItem.price);
                _localDesiredItem.dateWhenToBuyIt = _desiredItem.dateWhenToBuyIt;
                _localDesiredItem.idCategory = App.dbCategory.getIDategory(_desiredItem.category);
                _localDesiredItem.title = _desiredItem.title;
                if (_desiredItem.bought == "Tak")
                {
                    _localDesiredItem.bought = true;
                    _localDesiredItem.dateWhenWasBought = _desiredItem.dateWhenWasBought;
                }
                else
                {
                    _localDesiredItem.bought = false;
                }
                _localDesiredItem.amountCollected = Convert.ToDouble(_desiredItem.amountCollected);

                db.UpdateWithChildren(_localDesiredItem);
                App.LocalNotification("Zaktualizowano przedmiot");
                return true;
            }
            catch
            {
                App.LocalNotification("Wystąpił nieoczekiwany błąd podczas aktualizacji przedmiotu");
                return false;
            }
            
        }
    }
}
