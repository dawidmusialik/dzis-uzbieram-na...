﻿using DzisUzbieramNa.DataBase.Table;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DzisUzbieramNa.DataBase.Command
{
    public class CategoryCommand
    {
        private SQLiteConnection db;

        public CategoryCommand()
        {
            db = DataBase.Connection.Instance.getDataBaseConnection();
            CreateTable();

        }
        private void CreateTable()
        {
            try
            {
                var TableCategory = db.GetTableInfo("CategoryTable");

                if (TableCategory.Count == 0)
                {
                    db.CreateTable<CategoryTable>();
                    addDefault();
                    Console.WriteLine("Tabela CategoryTable została utworzona");
                }
                else
                {
                    Console.WriteLine("Tabela CategoryTable została już utworzona");
                }
            }

            catch (Exception ex)
            {
            }
        }
        private void addDefault()
        {
            List<CategoryTable> _listDefaultCategory = new List<CategoryTable>();
            _listDefaultCategory.Add(new CategoryTable() { category = "Bez kategorii" });
            _listDefaultCategory.Add(new CategoryTable() { category = "Elektronika" });
            _listDefaultCategory.Add(new CategoryTable() { category = "Motoryzacja" });
            _listDefaultCategory.Add(new CategoryTable() { category = "Dom" });
            
            foreach(var item in _listDefaultCategory)
            {
                db.Insert(item);
            }
        }

        public List<CategoryTable> getAllCategory()
        {
            var _allCategory = db.GetAllWithChildren<CategoryTable>();

            return _allCategory;
        }
        public string getStringCategory(int _idCategory)
        {
            try
            {
                string _category = db.GetAllWithChildren<CategoryTable>().Where(x => x.id == _idCategory).Select(x => x.category).FirstOrDefault();

                return _category;
            }
            catch
            {
                return "";
            }
            
        }
        public int getIDategory(string _category)
        {
            try
            {
                int _localCategory = db.GetAllWithChildren<CategoryTable>().Where(x => x.category == _category).Select(x => x.id).FirstOrDefault();

                return _localCategory;
            }
            catch
            {
                return 0;
            }
        }


        public bool addCategory(string category)
        {
            if(checkCategoryIsDataBase(category) == false)
            {
                var _category = new CategoryTable();
                _category.category = category;

                db.Insert(_category);
                App.LocalNotification("Dodano nową kategorię");
                return true;
            }
            else
            {
                App.LocalNotification("Kategoria już istnieje");
                return false;
            }
        }

        private bool checkCategoryIsDataBase(string _category)
        {
            string _isCategory = db.GetAllWithChildren<CategoryTable>().Where(x => x.category == _category).Select(x => x.category).FirstOrDefault();

            if (_isCategory != null)
            {
                if(_isCategory == _category)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        public bool updateCategory(Table.CategoryTable _category)
        {
            try
            {
                var _thisCategory = db.GetAllWithChildren<CategoryTable>().Where(x => x.id == _category.id).FirstOrDefault();
                _thisCategory.category = _category.category;
                db.UpdateWithChildren(_thisCategory);
                App.LocalNotification("Zmieniono nazwe kategorię");
                return true;
            }
            catch(Exception ex)
            {
                var t = ex.Message;
                App.LocalNotification("Nie zmieniono nazwy kategorii");
                return false;
            }
        }

        public bool deleteCategory(int _idCategory)
        {
            if(db.GetAllWithChildren<DesiredItemTable>().Where(x => x.idCategory == _idCategory).FirstOrDefault() == null)
            {
                if (db.GetAllWithChildren<CategoryTable>().Where(x => x.id == _idCategory).FirstOrDefault() != null)
                {
                    if(db.GetAllWithChildren<CategoryTable>().ToList().Count > 1)
                    {
                        db.Delete<CategoryTable>(_idCategory);
                        App.LocalNotification("Usunięto kategorię");
                        return true;
                    }
                    else
                    {
                        App.LocalNotification("Nie można usunąć ostatniej kategorii");
                        return false;

                    }

                }
                else
                {
                    App.LocalNotification("Nie znaleziono takiej kategorii, nieoczekiwany błąd!");
                    return false;
                }
            }
            else
            {
                App.LocalNotification("Nie można usunąć kategorii, jest ona obecnie w użyciu");
                return false;
            }
        }
    }
}
