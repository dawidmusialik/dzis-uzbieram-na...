﻿using SQLite.Net;
using SQLite.Net.Interop;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DzisUzbieramNa.DataBase
{
    public sealed class Connection
    {
        private static readonly object PadLock = new object();
        private static Connection _Instance = null;
        private SQLiteConnection dbConnectionSQL;

        private Connection()
        {
            createDatabase();
        }

        private void createDatabase()
        {
            try
            {
                ISQLitePlatform DatabaseNetPlatform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
                dbConnectionSQL = new SQLite.Net.SQLiteConnection(DatabaseNetPlatform, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "dbDzisUzbieramNa.db3"));
            }
            catch (Exception ex)
            {

            }
        }

        public static Connection Instance
        {
            get
            {
                lock (PadLock)
                {
                    if (_Instance == null)
                    {
                        _Instance = new Connection();
                    }
                    return _Instance;
                }
            }
        }

        public SQLiteConnection getDataBaseConnection()
        {
            return dbConnectionSQL;
        }
    }
}
