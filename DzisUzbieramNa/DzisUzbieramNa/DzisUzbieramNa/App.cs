﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

using DzisUzbieramNa.DataBase.Command;

namespace DzisUzbieramNa
{
    public partial class App : Application
    {
        public static CategoryCommand dbCategory;
        public static DesiredItemCommand dbDesiredItem;
        public static AmountCollectedCommand dbAmountCollected;

        public App()
        {
            dbCategory = new CategoryCommand();
            dbDesiredItem = new DesiredItemCommand();
            dbAmountCollected = new AmountCollectedCommand();
            MainPage = new NavigationPage( new View.DesiredItemsView());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
        public static void LocalNotification(string message)
        {
            DependencyService.Get<Interface.IDialog>().ShowDialog(message);
        }
    }
}
