﻿using DzisUzbieramNa.Interface;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DzisUzbieramNa.Services
{
    public class PhotoService : Interface.IPhoto
    {
        private MediaFile photo;
        private bool checkPhone = false;
        private bool _isPhotoExist = false;

        public PhotoService()
        {
            try
            {
                if (CrossMedia.Current.IsCameraAvailable == true && CrossMedia.Current.IsTakePhotoSupported == true)
                {
                    checkPhone = true;
                }
            }
            catch (Exception ex)
            {
                //ConfigurationSignleton.Instance.uriMailMessage(ex, " sprawdzanie dostępności aparatu ");
            }

            _isPhotoExist = false;
        }

        public void deletePhoto()
        {
            _isPhotoExist = false;
        }

        public void disposePhoto()
        {
            photo.Dispose();
        }

        public Image getImage()
        {
            return new Image()
            {
                Source = getImageString(),
            };
        }

        public string getImageString()
        {
            if (photo != null)
            {
                var t = photo.GetStream();//app directory
                var t1 = photo.AlbumPath;//local directory
                var t2 = photo.GetStreamWithImageRotatedForExternalStorage();
                return photo.Path;

            }
            else
            {
                return "";
            }
        }

        public bool deletePhoto_LocalPath()
        {
                if (File.Exists(photo.AlbumPath) == true)
                {
                    File.Delete(photo.AlbumPath);
                    return true;
                }
                else return false;
        }
        public bool deletePhoto_AppPath()
        {
            if (File.Exists(((FileStream)photo.GetStream()).Name) == true)
            {
                File.Delete(((FileStream)photo.GetStream()).Name);
                return true;
            }
            else return false;
        }

        public async Task takePhoto()
        {
            if (checkPhone == true && _isPhotoExist == false)
            {
                try
                {
                    photo = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                    {
                        Directory = "Dziś Uzbieram Na",
                        AllowCropping = true,
                        SaveToAlbum = true,
                        CompressionQuality = 75,
                        CustomPhotoSize = 50,
                        PhotoSize = PhotoSize.MaxWidthHeight,
                        MaxWidthHeight = 2000,
                        DefaultCamera = CameraDevice.Rear
                    });
                }
                catch (Exception ex)
                {
                   
                }
                finally
                {
                    if (photo != null)
                    {
                        _isPhotoExist = true;
                    }
                }
            }
        }

        public bool isPhotoExist()
        {
            return _isPhotoExist;
        }
    }
}
