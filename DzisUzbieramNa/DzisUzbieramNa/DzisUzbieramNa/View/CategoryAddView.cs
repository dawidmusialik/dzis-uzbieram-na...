﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.View
{
    public class CategoryAddView:ContentPage
    {
        StackLayout _stackLayout;
        ViewModel.CategoryAddViewModel _viewElements;
        
        public CategoryAddView()
        {
            createElements();
            setElements();
        }

        private void createElements()
        {
            _stackLayout = new StackLayout();
            _viewElements = new ViewModel.CategoryAddViewModel();
        }

        private void setElements()
        {
            _stackLayout.HorizontalOptions = LayoutOptions.CenterAndExpand;

            _stackLayout.Children.Add(_viewElements.labelName);
            _stackLayout.Children.Add(_viewElements.entryCategory);
            _stackLayout.Children.Add(_viewElements.buttonSaveCategory);


            this.Title = "Dodaj kategorię";
            this.Content = _stackLayout;
        }

       
        protected override void OnDisappearing()
        {
            if (((NavigationPage)App.Current.MainPage).RootPage.GetType().Name == nameof(DzisUzbieramNa.View.CategoryView))
            {
                ((View.CategoryView)((NavigationPage)App.Current.MainPage).RootPage)._listView.ItemsSource = App.dbCategory.getAllCategory();
            }

                base.OnDisappearing();
        }
    }
}
