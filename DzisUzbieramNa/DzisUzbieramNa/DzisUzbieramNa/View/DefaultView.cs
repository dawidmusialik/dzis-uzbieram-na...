﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace DzisUzbieramNa.View
{
    public class DefaultView:ContentPage
    {

        public DefaultView()
        {
            this.Content = new StackLayout()
            {
                Children =
                {
                    new Label()
                    {
                        Text = "Dziś Uzbieram Na...",
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                    },
                }
            };
            this.Title = "Dziś uzbieram na...";
        }

    }
}
