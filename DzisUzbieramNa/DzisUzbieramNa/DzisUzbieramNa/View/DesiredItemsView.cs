﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.View
{
    public class DesiredItemsView:ContentPage
    {
        StackLayout _stackLayout;
        public ListView _listView;
        ToolbarItem _toolbarItemAdd;
        ToolbarItem _toolbarItemCategory;

        //ToolbarItem _toolbarItemEdit;
        //ToolbarItem _toolbarItemDelete;

        public DesiredItemsView()
        {
            createItem();
            setItem();
        }
        private void createItem()
        {
            _stackLayout = new StackLayout();
            _listView = new ListView();
            _toolbarItemAdd = new ToolbarItem();
            _toolbarItemCategory = new ToolbarItem();

            //_toolbarItemEdit = new ToolbarItem();
            //_toolbarItemDelete = new ToolbarItem();
        }
        private void setItem()
        {
            _toolbarItemAdd.Text = "Dodaj";
            _toolbarItemAdd.Icon = "iconAdd.png";
            _toolbarItemAdd.Clicked += (sender, e) =>
            {
                this.ToolbarItems.Clear();
                App.Current.MainPage.Navigation.PushAsync(new View.DesiredItemAdd());
            };
            this.ToolbarItems.Add(_toolbarItemAdd);

            _toolbarItemCategory.Text = "Kategorie";
            _toolbarItemCategory.Clicked += (sender, e) =>
            {
                this.ToolbarItems.Clear();
                App.Current.MainPage.Navigation.PushAsync(new View.CategoryView());
            };
            this.ToolbarItems.Add(_toolbarItemCategory);

            //_toolbarItemEdit.Text = "Edytuj";
            //_toolbarItemEdit.Icon = "iconEdit.png";
            //_toolbarItemDelete.Text = "Usuń";
            //_toolbarItemDelete.Icon = "iconDelete.png";

            _listView.ItemTemplate = new DataTemplate(typeof(ViewModel.DesiredItemViewCell));
            _listView.ItemsSource = App.dbDesiredItem.getAllDesiredItemShort();
            _listView.HasUnevenRows = true;
            _listView.RowHeight = -1;
            _listView.ItemTapped += (sender, e) =>
            {
                if (_listView.SelectedItem != null)
                {
                    //App.Current.MainPage.Navigation.PushAsync(new View.DesiredItemView(((Model.DesiredItemModelShort)e.Item).id));
                    App.Current.MainPage.Navigation.PushAsync(new View.DesiredItemView(((Model.DesiredItemModelShort)_listView.SelectedItem).id));

                    _listView.SelectedItem = null;
                }

                //if (((Model.DesiredItemModelShort)e.Item) != null)
                //{
                //    App.Current.MainPage.Navigation.PushAsync(new View.DesiredItemView(((Model.DesiredItemModelShort)e.Item).id));
                //}
            };
            
            //_listView.ItemSelected += (sender, e) =>
            //{
            //    if(((Model.DesiredItemModelShort)e.SelectedItem) != null)
            //    {
            //        //this.ToolbarItems.Clear();
            //        //if (this.ToolbarItems.Count == 0)
            //        //{
            //        //    this.ToolbarItems.Add(_toolbarItemAdd);
            //        //}
            //        if (this.ToolbarItems.Count > 1)
            //        {
            //            this.ToolbarItems.Remove(_toolbarItemEdit);
            //            this.ToolbarItems.Remove(_toolbarItemDelete);
            //        }
                        
            //        _toolbarItemEdit.Clicked += (senderEdit, eEdit) =>
            //        {
            //            var _currentDesiredItem = ((Model.DesiredItemModelShort)e.SelectedItem);
            //            //App.Current.MainPage.Navigation.PushAsync(new View.CategoryEditView(_currentCategory));

            //            //this.ToolbarItems.Clear();
            //            //if (this.ToolbarItems.Count == 0)
            //            //{
            //            //    this.ToolbarItems.Add(_toolbarItemAdd);
            //            //}
            //            this.ToolbarItems.Remove(_toolbarItemEdit);
            //            this.ToolbarItems.Remove(_toolbarItemDelete);

            //            this._listView.SelectedItem = null;
            //        };

            //        _toolbarItemDelete.Clicked += (senderEdit, eEdit) =>
            //        {
            //            var _currentDesiredItem = ((Model.DesiredItemModelShort)e.SelectedItem);
            //            var _status = App.dbDesiredItem.deleteDesiredItem(_currentDesiredItem.id);
            //            if (_status == true)
            //            {
            //                //this.ToolbarItems.Clear();
            //                //if (this.ToolbarItems.Count == 0)
            //                //{
            //                //    this.ToolbarItems.Add(_toolbarItemAdd);
            //                //}
            //                this.ToolbarItems.Remove(_toolbarItemEdit);
            //                this.ToolbarItems.Remove(_toolbarItemDelete);

            //                _listView.ItemsSource = App.dbDesiredItem.getAllDesiredItemShort();
            //            }
            //            else
            //            {
            //                //this.ToolbarItems.Clear();
            //                //if (this.ToolbarItems.Count == 0)
            //                //{
            //                //    this.ToolbarItems.Add(_toolbarItemAdd);
            //                //}
            //                this.ToolbarItems.Remove(_toolbarItemEdit);
            //                this.ToolbarItems.Remove(_toolbarItemDelete);
            //            }

            //            this._listView.SelectedItem = null;
            //        };


            //        this.ToolbarItems.Add(_toolbarItemEdit);
            //        this.ToolbarItems.Add(_toolbarItemDelete);
            //    }
               
                
            //};




            _stackLayout.Margin = new Thickness(5, 5, 5, 5);
            _stackLayout.Children.Add(_listView);
            this.Content = _stackLayout;
            this.Title = "Dziś uzbieram na...";
        }

        

        protected override void OnAppearing()
        {
            if (this.ToolbarItems.Count == 0)
            {
                this.ToolbarItems.Add(_toolbarItemAdd);
                this.ToolbarItems.Add(_toolbarItemCategory);

            }

            if(ViewModel.AmountCollectedViewModel_ControlPanel.isNowItemAmountCollected == true)
            {
                _listView.ItemsSource = App.dbDesiredItem.getAllDesiredItemShort();
                ViewModel.AmountCollectedViewModel_ControlPanel.isNowItemAmountCollected = false;
            }

            if (ViewModel.DesiredItemAddViewModel.idNewItemOrUpdatedItem == true )
            {
                _listView.ItemsSource = App.dbDesiredItem.getAllDesiredItemShort();
                ViewModel.DesiredItemAddViewModel.idNewItemOrUpdatedItem = false;
            }

            if(View.DesiredItemView.isNewItemOrUpdatedItem == true)
            {
                _listView.ItemsSource = App.dbDesiredItem.getAllDesiredItemShort();
                View.DesiredItemView.isNewItemOrUpdatedItem = false;
            }

            //this._listView.SelectedItem = null;

            base.OnAppearing();
        }
        protected override void OnDisappearing()
        {
            this.ToolbarItems.Clear();

            base.OnDisappearing();
        }
    }
}
