﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.View
{
    public class CategoryView: ContentPage
    {
        StackLayout _stackLayout;
        public ListView _listView;
        ToolbarItem _toolbarItemAdd;
        ToolbarItem _toolbarItemEdit;
        ToolbarItem _toolbarItemDelete;
        public CategoryView()
        {
            createItem();
            setItem();
        }

        private void createItem()
        {
            _stackLayout = new StackLayout();
            _listView = new ListView();
            _toolbarItemAdd = new ToolbarItem();
            _toolbarItemEdit = new ToolbarItem();
            _toolbarItemDelete = new ToolbarItem();
        }

        private void setItem()
        {
            _toolbarItemAdd.Text = "Dodaj";
            _toolbarItemAdd.Icon = "iconAdd.png";
            _toolbarItemAdd.Clicked += (sender, e) =>
            {
                this.ToolbarItems.Clear();
                App.Current.MainPage.Navigation.PushAsync(new View.CategoryAddView());
            };
            this.ToolbarItems.Add(_toolbarItemAdd);

            _toolbarItemEdit.Text = "Edytuj";
            _toolbarItemEdit.Icon = "iconEdit.png";
            _toolbarItemDelete.Text = "Usuń";
            _toolbarItemDelete.Icon = "iconDelete.png";

            _listView.ItemTemplate = new DataTemplate(typeof(ViewModel.CategoryViewCell));
            
            _listView.ItemsSource = App.dbCategory.getAllCategory();
            _listView.HasUnevenRows = true;
            _listView.RowHeight = -1;
            

            _listView.ItemSelected += (sender, e) => 
            {
               // if (((DataBase.Table.CategoryTable)e.SelectedItem) != null)
               if(_listView.SelectedItem != null)
                {
                    _toolbarItemEdit.Clicked += (senderEdit, eEdit) =>
                    {
                        if (_listView.SelectedItem != null)
                        {
                            var _currentCategory = ((DataBase.Table.CategoryTable)_listView.SelectedItem);
                            App.Current.MainPage.Navigation.PushAsync(new View.CategoryEditView(_currentCategory));


                            this.ToolbarItems.Remove(_toolbarItemEdit);
                            this.ToolbarItems.Remove(_toolbarItemDelete);

                            this._listView.SelectedItem = null;
                        }
                    };

                    _toolbarItemDelete.Clicked += (senderDelete, eDelete) =>
                    {
                        if (_listView.SelectedItem != null)
                        {
                            var _currentCategory = ((DataBase.Table.CategoryTable)_listView.SelectedItem);
                            var _status = App.dbCategory.deleteCategory(_currentCategory.id);
                            if (_status == true)
                            {
                                this.ToolbarItems.Remove(_toolbarItemEdit);
                                this.ToolbarItems.Remove(_toolbarItemDelete);

                                _listView.ItemsSource = App.dbCategory.getAllCategory();
                            }
                            else
                            {
                                this.ToolbarItems.Remove(_toolbarItemEdit);
                                this.ToolbarItems.Remove(_toolbarItemDelete);
                            }

                            this._listView.SelectedItem = null;
                        }
                    };


                    if (this.ToolbarItems.Count > 1)
                    {
                        this.ToolbarItems.Remove(_toolbarItemEdit);
                        this.ToolbarItems.Remove(_toolbarItemDelete);
                    }

                    this.ToolbarItems.Add(_toolbarItemEdit);
                    this.ToolbarItems.Add(_toolbarItemDelete);
                }

            };
           

            _stackLayout.Margin = new Thickness(5, 5, 5, 5);
            _stackLayout.Children.Add(_listView);
            this.Content = _stackLayout;
            this.Title = "Kategorie";
        }

        protected override void OnAppearing()
        {
            if(this.ToolbarItems.Count == 0)
            {
                this.ToolbarItems.Add(_toolbarItemAdd);
            }

            if(ViewModel.CategoryEditViewModel.isNewItemOrUpdatedItem == true)
            {
                _listView.ItemsSource = App.dbCategory.getAllCategory();
                ViewModel.CategoryEditViewModel.isNewItemOrUpdatedItem = false;
            }

            if(ViewModel.CategoryAddViewModel.isNewElements == true)
            {
                _listView.ItemsSource = App.dbCategory.getAllCategory();
                ViewModel.CategoryAddViewModel.isNewElements = false;
            }

            //this._listView.SelectedItem = null; 

            base.OnAppearing();
        }
        protected override void OnDisappearing()
        {
            this.ToolbarItems.Clear();

            base.OnDisappearing();
        }

    }
}
