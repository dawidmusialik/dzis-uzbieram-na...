﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.View
{
    public class DesiredItemEditView:ContentPage
    {
        StackLayout _stackLayout;
        ScrollView _scrollView;
        ViewModel.DesiredItemEditViewModel _viewElements;

        private int idDesiredItem;

        public DesiredItemEditView(int _idDesiredItem)
        {
            idDesiredItem = _idDesiredItem;
            createElements(_idDesiredItem);
            setElements();
        }

        private void createElements(int _idDesiredItem)
        {
            _stackLayout = new StackLayout();
            _scrollView = new ScrollView();
            _viewElements = new ViewModel.DesiredItemEditViewModel(_idDesiredItem);
        }

        private void setElements()
        {
            _stackLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Vertical,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        Children =
                        {
                            _viewElements.labelTitlenName,
                            _viewElements.entryTitle,
                        }
                    },
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Vertical,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        Children =
                        {
                            _viewElements.labelCategoryName,
                            _viewElements.pickerCategory,
                        }
                    },
                }
            });

            _stackLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    new StackLayout()
                    {
                       Orientation = StackOrientation.Vertical,
                       Children =
                       {
                            _viewElements.imageDesiredItem,
                            _viewElements.buttonChangeImage,
                       }
                    },
                    new StackLayout()
                    {
                       Orientation = StackOrientation.Vertical,
                       Children =
                       {
                            new StackLayout()
                            {
                               Orientation = StackOrientation.Vertical,
                               HorizontalOptions = LayoutOptions.CenterAndExpand,
                               Children =
                               {
                                    _viewElements.labelPriceName,
                                    _viewElements.entryPrice,
                               }
                            },
                             new StackLayout()
                            {
                               Orientation = StackOrientation.Horizontal,
                               HorizontalOptions = LayoutOptions.CenterAndExpand,
                               Children =
                               {
                                    _viewElements.labelAmountName,
                                    _viewElements.labelAmount,
                               }
                            },
                              new StackLayout()
                            {
                               Orientation = StackOrientation.Vertical,
                               HorizontalOptions = LayoutOptions.CenterAndExpand,
                               Children =
                               {
                                    _viewElements.labelDateWhenToBuyItName,
                                    _viewElements.datePickerDateWhenToBuyIt,
                               }
                            },
                               new StackLayout()
                            {
                               Orientation = StackOrientation.Vertical,
                               HorizontalOptions = LayoutOptions.CenterAndExpand,
                               Children =
                               {
                                    _viewElements.labelDateWhenWasBoughtName,
                                    _viewElements.datePickerDateWhenWasBought,
                               }
                            },
                                new StackLayout()
                            {
                               Orientation = StackOrientation.Vertical,
                               HorizontalOptions = LayoutOptions.CenterAndExpand,
                               Children =
                               {
                                    _viewElements.labelDescriptionName,
                                    _viewElements.editorDescription,
                               }
                            },
                                 new StackLayout()
                            {
                               Orientation = StackOrientation.Vertical,
                               HorizontalOptions = LayoutOptions.CenterAndExpand,
                               Children =
                               {
                                    _viewElements.labelBoughtName,
                                    _viewElements.switchBought,
                               }
                            },
                       }
                    },
                }
            });

            _stackLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children =
                {
                    _viewElements.labelDescriptionName,
                    _viewElements.editorDescription,
                }
            });

            _stackLayout.Children.Add(_viewElements.buttonSave);



            _scrollView.Content = _stackLayout;
            this.Title = "Edytuj przedmiot";
            this.Content = _scrollView;
        }

        protected override void OnDisappearing()
        {

            if(_viewElements._photoService.isPhotoExist() == true)
            {
                        _viewElements._photoService.deletePhoto_AppPath();
                        _viewElements._photoService.deletePhoto_LocalPath();
                        ViewModel.DesiredItemEditViewModel.isNewPhoto = false;
                        _viewElements._photoService.deletePhoto();
            }
         
            base.OnDisappearing();
        }

        protected override bool OnBackButtonPressed()
        {
            
            return base.OnBackButtonPressed();
        }

    }
}
