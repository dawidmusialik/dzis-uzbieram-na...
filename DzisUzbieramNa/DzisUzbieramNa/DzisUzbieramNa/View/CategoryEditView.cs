﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.View
{
    public class CategoryEditView:ContentPage
    {
        StackLayout _stackLayout;
        ViewModel.CategoryEditViewModel _viewElements;

        public CategoryEditView(DataBase.Table.CategoryTable _category)
        {
            createElements(_category);
            setElements();
        }

        private void createElements(DataBase.Table.CategoryTable _category)
        {
            _stackLayout = new StackLayout();
            _viewElements = new ViewModel.CategoryEditViewModel(_category);
        }

        private void setElements()
        {
            _stackLayout.HorizontalOptions = LayoutOptions.CenterAndExpand;

            _stackLayout.Children.Add(_viewElements.labelNameOld);

            _stackLayout.Children.Add(_viewElements.labelNameNew);
            _stackLayout.Children.Add(_viewElements.entryCategoryNew);
            _stackLayout.Children.Add(_viewElements.buttonSaveCategory);


            this.Title = "Dodaj kategorię";
            this.Content = _stackLayout;
        }


        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}
