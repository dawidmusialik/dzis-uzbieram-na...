﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.View
{
    public class DesiredItemView:ContentPage
    {
        StackLayout _stackLayout;
        ScrollView _scrollView;
        public ListView _listViewAmount;
        public ViewModel.DesiredItemViewModel _viewElements;
        ViewModel.AmountCollectedViewModel_ControlPanel _viewElementsAmountPanel;
        ToolbarItem _toolbarItemEdit;
        ToolbarItem _toolbarItemDelete;
        int _localIDDesiresItem;
        public static bool isNewItemOrUpdatedItem = false;


        public DesiredItemView(int _idDesiredItem)
        {
            createItem(_idDesiredItem);
            setElements(_idDesiredItem);
        }

        private void createItem(int _idDesiredItem)
        {
            _localIDDesiresItem = _idDesiredItem;

            _stackLayout = new StackLayout();
            _scrollView = new ScrollView();

            _viewElements = new ViewModel.DesiredItemViewModel(_idDesiredItem);
            _viewElementsAmountPanel = new ViewModel.AmountCollectedViewModel_ControlPanel(_idDesiredItem);

            _toolbarItemEdit = new ToolbarItem();
            _toolbarItemDelete = new ToolbarItem();

            _listViewAmount = new ListView();
        }

        private void setElements(int _idDesiredItem)
        {
            _toolbarItemEdit.Text = "Edytuj";
            _toolbarItemEdit.Icon = "iconEdit.png";
            _toolbarItemDelete.Text = "Usuń";
            _toolbarItemDelete.Icon = "iconDelete.png";

            _toolbarItemEdit.Clicked += (senderEdit, eEdit) =>
            {
                this.ToolbarItems.Clear();
                App.Current.MainPage.Navigation.PushAsync(new View.DesiredItemEditView(_idDesiredItem));
            };

            _toolbarItemDelete.Clicked += (senderEdit, eEdit) =>
            {
                var _status = App.dbDesiredItem.deleteDesiredItem(_localIDDesiresItem);
                if (_status == true)
                {
                    isNewItemOrUpdatedItem = true;

                    App.Current.MainPage.Navigation.PopAsync();
                }
            };

            _listViewAmount.ItemsSource = App.dbAmountCollected.getAllAmountCollected(_idDesiredItem);
            _listViewAmount.ItemTemplate = new DataTemplate(typeof(ViewModel.AmountCollectedViewCell));
            _listViewAmount.HasUnevenRows = true;
            _listViewAmount.RowHeight = -1;
            _listViewAmount.VerticalOptions = LayoutOptions.Fill;
            _listViewAmount.ItemTapped += (sender, e) =>
            {
                _listViewAmount.SelectedItem = null;
            };

            _stackLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                //BackgroundColor = Color.Red,
                Children =
                {
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        //BackgroundColor = Color.Blue,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        Children =
                        {
                            _viewElements.labelTitlenName,
                            _viewElements.labelTitle,
                        }

                    },
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        //BackgroundColor = Color.Yellow,
                        Children =
                        {
                            _viewElements.labelCategoryName,
                            _viewElements.labelCategory,
                        }

                    },
                    
                }
            });

            _stackLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Vertical,
                        Children =
                        {
                            _viewElements.imageDesiredItem,
                        }

                    },
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Vertical,
                        Children =
                        {
                            new StackLayout()
                            {
                                Orientation = StackOrientation.Horizontal,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,

                                Children =
                                {
                                    _viewElements.labelPriceName,
                                    _viewElements.labelPrice,
                                }

                            },
                            new StackLayout()
                            {
                                Orientation = StackOrientation.Horizontal,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,

                                Children =
                                {
                                    _viewElements.labelAmountName,
                                    _viewElements.labelAmount,
                                }
                            },
                            new StackLayout()
                            {
                                Orientation = StackOrientation.Horizontal,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,
                                Children =
                                {
                                    _viewElements.labelBoughtName,
                                    _viewElements.labelBought,
                                }
                            },
                            new StackLayout()
                            {
                                Orientation = StackOrientation.Vertical,
                                //BackgroundColor = Color.Red,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,
                                Margin = new Thickness(0, 15, 0, 0),
                                Children =
                                {
                                    _viewElements.labelDateWhenToBuyItName,
                                    _viewElements.labelDateWhenToBuyIt,
                                }
                            },
                            new StackLayout()
                            {
                                Orientation = StackOrientation.Vertical,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,
                                Children =
                                {
                                    _viewElements.labelDateWhenWasBoughtName,
                                    _viewElements.labelDateWhenWasBought,
                                }
                            },
                        }

                    },

                }
            });

            _stackLayout.Children.Add(
            new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                Children =
                {
                        _viewElements.labelDescriptionName,
                        _viewElements.labelDescription,
                },
                
            });


            StackLayout _stackLayoutSecond = new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                Margin = new Thickness(10, 5, 10 ,5),
                Children =
                {
                    _stackLayout,
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        //BackgroundColor = Color.Yellow,
                        Margin = new Thickness(0, 20, 0, 0),
                        Children =
                        {
                            _viewElementsAmountPanel.labelValue,
                            _viewElementsAmountPanel.entryValue,
                            _viewElementsAmountPanel.buttonAddition,
                            _viewElementsAmountPanel.buttonSubtraction,
                        },
                    },
                   // _listViewAmount,
                }
            };
            

            _scrollView.Content = _listViewAmount;
            _stackLayoutSecond.Children.Add(_scrollView);


            this.Content = _stackLayoutSecond;
            this.Title = App.dbDesiredItem.getDesiredItemTitle(_idDesiredItem);


            if (this.ToolbarItems.Count == 0)
            {
                this.ToolbarItems.Add(_toolbarItemEdit);
                this.ToolbarItems.Add(_toolbarItemDelete);
            }
        }
        protected override void OnAppearing()
        {
            
            if (this.ToolbarItems.Count == 0)
            {
                this.ToolbarItems.Add(_toolbarItemEdit);
                this.ToolbarItems.Add(_toolbarItemDelete);
            }

            if(ViewModel.DesiredItemEditViewModel.isEditedItem == true)
            {

                ViewModel.DesiredItemEditViewModel.isEditedItem = false;
            }
            base.OnAppearing();
        }
        protected override void OnDisappearing()
        {
            this.ToolbarItems.Clear();

            base.OnDisappearing();
        }

        protected override bool OnBackButtonPressed()
        {
            //if (((NavigationPage)App.Current.MainPage).RootPage.GetType().Name == nameof(DzisUzbieramNa.View.DesiredItemsView))
            //{
            //    ((View.DesiredItemsView)((NavigationPage)App.Current.MainPage).RootPage)._listView.ItemsSource = App.dbDesiredItem.getAllDesiredItemShort();
            //}
            return base.OnBackButtonPressed();
        }


    }
}
