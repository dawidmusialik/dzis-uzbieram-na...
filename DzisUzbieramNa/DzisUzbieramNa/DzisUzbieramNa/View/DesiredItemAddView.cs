﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.View
{
    public class DesiredItemAdd:ContentPage
    {
        StackLayout _stackLayout;
        ScrollView _scrollView;
        ViewModel.DesiredItemAddViewModel _viewElements;

        public DesiredItemAdd()
        {
            createItem();
            setItem();
        }
        private void createItem()
        {
            _stackLayout = new StackLayout();
            _scrollView = new ScrollView();
            _viewElements = new ViewModel.DesiredItemAddViewModel();
        }
        private void setItem()
        {
            _stackLayout.Margin = new Thickness(10, 15, 10, 15);
            _stackLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children =
                {
                    _viewElements.labelDesiredItemTitle,
                    _viewElements.entryDesiredItemTitle,
                }
            });
            

            _stackLayout.Children.Add(_viewElements.labelDesiredItemDescription);
            _stackLayout.Children.Add(_viewElements.editorDesiredItemDescription);

            _stackLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children =
                {
                    _viewElements.labelDesiredItemPrice,
                    _viewElements.entryDesiredItemPrice
                }
            });

            _stackLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children =
                {
                    _viewElements.labelDesiredItemCategory,
                    _viewElements.pickerDesiredItemCategory
                }
            });
            

            _stackLayout.Children.Add(_viewElements.buttonTakePhoto);
            _stackLayout.Children.Add(_viewElements.imageDesiredItemImage);

            _stackLayout.Children.Add(_viewElements.buttonSaveDesiredItem);

            _scrollView.Content = _stackLayout;
            this.Content = _scrollView;
            this.Title = "Dodaj nowy element";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        protected override void OnDisappearing()
        {
            //if (((NavigationPage)App.Current.MainPage).RootPage.GetType().Name == nameof(DzisUzbieramNa.View.DesiredItemView))
            //{
            //    ((View.DesiredItemView)((NavigationPage)App.Current.MainPage).RootPage)._listView.ItemsSource = App.dbDesiredItem.getAllDesiredItem();
            //}

            if(_viewElements._photoService.isPhotoExist() == true)
            {
                _viewElements._photoService.deletePhoto_AppPath();
                _viewElements._photoService.deletePhoto_LocalPath();
                _viewElements._photoService.deletePhoto();
            }

            base.OnDisappearing();
        }
    }
}
