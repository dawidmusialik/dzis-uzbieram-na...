﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Linq;

namespace DzisUzbieramNa.ViewModel
{
    class CategoryEditViewModel
    {
        public Label labelNameOld { get; set; }

        public Label labelNameNew { get; set; }
        public Entry entryCategoryNew { get; set; }
        public Button buttonSaveCategory { get; set; }

        public static bool isNewItemOrUpdatedItem = false;
        public CategoryEditViewModel( DataBase.Table.CategoryTable _category)
        {
            createItem();
            setItem(_category);
        }

        private void createItem()
        {
            labelNameNew = new Label();
            labelNameOld = new Label();
            entryCategoryNew = new Entry();
            buttonSaveCategory = new Button();
        }

        private void setItem(DataBase.Table.CategoryTable _category)
        {
            labelNameNew.Text = "Nowa nazwa kategorii: ";
            labelNameNew.HorizontalOptions = LayoutOptions.CenterAndExpand;
            labelNameNew.HorizontalTextAlignment = TextAlignment.Center;
            labelNameNew.VerticalTextAlignment = TextAlignment.Center;
            labelNameNew.Margin = new Thickness(0, 10, 0, 5);

            labelNameOld.Text = "Stara nazwa kategorii: " + _category.category;
            labelNameOld.HorizontalOptions = LayoutOptions.CenterAndExpand;
            labelNameOld.HorizontalTextAlignment = TextAlignment.Center;
            labelNameOld.VerticalTextAlignment = TextAlignment.Center;
            labelNameOld.Margin = new Thickness(0, 10, 0, 20);

            entryCategoryNew.Placeholder = "Wprowadź nową nazwę";
            entryCategoryNew.HorizontalOptions = LayoutOptions.CenterAndExpand;
            entryCategoryNew.HorizontalTextAlignment = TextAlignment.Center;
            entryCategoryNew.Margin = new Thickness(0, 5, 0, 10);

            buttonSaveCategory.Text = "Aktualizuj";
            buttonSaveCategory.HorizontalOptions = LayoutOptions.CenterAndExpand;
            buttonSaveCategory.Margin = new Thickness(0, 15, 0, 10);

            setButtonClicked(_category);
        }

        private void setButtonClicked(DataBase.Table.CategoryTable _category)
        {
            buttonSaveCategory.Clicked += (sender, e) =>
            {
                if (entryCategoryNew.Text != null && entryCategoryNew.Text != "" && entryCategoryNew.Text != _category.category)
                {
                    _category.category = entryCategoryNew.Text;
                    bool _status = App.dbCategory.updateCategory(_category);
                    if (_status == true)
                    {

                        labelNameOld.Text = "Stara nazwa kategorii: " + _category.category;
                        entryCategoryNew.Text = null;
                        entryCategoryNew.Placeholder = "Wprowadź nową nazwę";
                        isNewItemOrUpdatedItem = true;
                    }
                }
                else
                {
                    App.LocalNotification("Próbujesz zapisać tą samą nazwę kategorii");
                }
            };
        }

    }
}
