﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.ViewModel
{
    public class DesiredItemsViewModel
    {

        public Image imageDesiredItem { get; set; }

        public Label labelTitlenName { get; set; }
        public Label labelTitle { get; set; }

        public Label labelDescriptionName { get; set; }
        public Label labelDescription { get; set; }

        public Label labelPriceName { get; set; }
        public Label labelPrice { get; set; }

        public Label labelAmountName { get; set; }
        public Label labelAmount { get; set; }

        public Label labelCategoryName { get; set; }
        public Label labelCategory { get; set; }
       

        public DesiredItemsViewModel()
        {
            createItem();
            setItem();
        }

        private void createItem()
        {
            imageDesiredItem = new Image();
            
            labelDescriptionName = new Label();
            labelDescription = new Label();

            labelTitle = new Label();
            labelTitlenName = new Label();

            labelPriceName = new Label();
            labelPrice = new Label();

            labelAmountName = new Label();
            labelAmount = new Label();

            labelCategoryName = new Label();
            labelCategory = new Label();

        }

        private void setItem()
        {
            imageDesiredItem.HeightRequest = 100;
            imageDesiredItem.WidthRequest = 100;

            labelDescriptionName.Text = "Opis: ";
            labelDescriptionName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            labelDescriptionName.HorizontalTextAlignment = TextAlignment.Center;
            labelDescription.HorizontalOptions = LayoutOptions.CenterAndExpand;

            labelPriceName.Text = "Cena: ";
            labelPriceName.HorizontalOptions = LayoutOptions.Start;
            labelPrice.HorizontalOptions = LayoutOptions.StartAndExpand;

            labelAmountName.Text = "Uzbierano: ";
            labelAmountName.HorizontalOptions = LayoutOptions.End;
            labelAmount.HorizontalOptions = LayoutOptions.EndAndExpand;

            labelTitlenName.Text = "Nazwa: ";
            labelTitlenName.HorizontalOptions = LayoutOptions.Start;
            labelTitlenName.FontAttributes = FontAttributes.Bold;
            labelTitle.HorizontalOptions = LayoutOptions.StartAndExpand;
            labelTitle.FontAttributes = FontAttributes.Bold;

            labelCategoryName.Text = "Kategoria: ";
            labelCategoryName.HorizontalOptions = LayoutOptions.EndAndExpand;
            labelCategoryName.FontAttributes = FontAttributes.Bold;
            labelCategory.HorizontalOptions = LayoutOptions.End;
            labelCategory.FontAttributes = FontAttributes.Bold;
        }
    }

    public class DesiredItemViewCell: ViewCell
    {
        DesiredItemsViewModel _viewElements { get; set; }
        StackLayout _stackLayout { get; set; }
        public DesiredItemViewCell()
        {
            createElements();
            setElements();
        }
        private void createElements()
        {
            _viewElements = new DesiredItemsViewModel();
            _stackLayout = new StackLayout();
        }

        private void setElements()
        {
            _stackLayout.Margin = new Thickness(0, 0, 0, 5);

            _viewElements.labelTitle.SetBinding(Label.TextProperty, new Binding(path: "title"));
            _viewElements.imageDesiredItem.SetBinding(Image.SourceProperty, new Binding(path: "imagePath"));
            _viewElements.labelDescription.SetBinding(Label.TextProperty, new Binding(path: "description"));
            _viewElements.labelPrice.SetBinding(Label.TextProperty, new Binding(path: "price"));
            _viewElements.labelCategory.SetBinding(Label.TextProperty, new Binding(path: "category"));
            _viewElements.labelAmount.SetBinding(Label.TextProperty, new Binding(path: "amountCollected"));
            _stackLayout.SetBinding(StackLayout.BackgroundColorProperty, new Binding(path: "isBought"));


            _stackLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Children =
                        {

                            new StackLayout()
                            {
                                Orientation = StackOrientation.Horizontal,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,
                                Children =
                                {
                                    _viewElements.labelTitlenName,
                                    _viewElements.labelTitle,
                                }
                            },
                            new StackLayout()
                            {
                                Orientation = StackOrientation.Horizontal,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,
                                Children =
                                {
                                    _viewElements.labelCategoryName,
                                    _viewElements.labelCategory,
                                }
                            },
                        }
            }
            );

            _stackLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                //HorizontalOptions = LayoutOptions.StartAndExpand,
               // BackgroundColor = Color.Green,
                Children =
                {
                    _viewElements.imageDesiredItem,
                    new StackLayout()
                    {
                        Orientation = StackOrientation.Vertical,
                        //BackgroundColor = Color.Red,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Children =
                        {

                            new StackLayout()
                            {
                                Orientation = StackOrientation.Horizontal,
                                HorizontalOptions = LayoutOptions.Fill,
                                //BackgroundColor = Color.Blue,
                                Children =
                                {

                                    new StackLayout()
                                    {
                                        Orientation = StackOrientation.Horizontal,
                                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                                        Children =
                                        {
                                            _viewElements.labelPriceName,
                                            _viewElements.labelPrice,
                                        }
                                    },
                                    new StackLayout()
                                    {
                                        Orientation = StackOrientation.Horizontal,
                                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                                        Children =
                                        {
                                            _viewElements.labelAmountName,
                                            _viewElements.labelAmount,
                                        }
                                    },
                                }
                            },
                            new StackLayout()
                            {
                                Orientation = StackOrientation.Vertical,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,
                                //BackgroundColor = Color.Yellow,
                                Children =
                                {
                                    _viewElements.labelDescriptionName,
                                    _viewElements.labelDescription,
                                }
                            },
                        }
                    }

                },
            });
            
            this.View = _stackLayout;
        }
        
    }
}
