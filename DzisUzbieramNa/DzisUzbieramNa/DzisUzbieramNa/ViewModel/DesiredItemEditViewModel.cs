﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace DzisUzbieramNa.ViewModel
{
    public class DesiredItemEditViewModel
    {
        public Image imageDesiredItem { get; set; }
        public Button buttonChangeImage { get; set; }
        private string imagePathToDelete;

        public Label labelTitlenName { get; set; }
        public Entry entryTitle { get; set; }

        public Label labelDescriptionName { get; set; }
        public Editor editorDescription { get; set; }

        public Label labelPriceName { get; set; }
        public Entry entryPrice { get; set; }

        public Label labelAmountName { get; set; }
        public Label labelAmount { get; set; }

        public Label labelCategoryName { get; set; }
        public Picker pickerCategory { get; set; }

        public Label labelBoughtName { get; set; }
        public Switch switchBought { get; set; }

        public Label labelDateWhenToBuyItName { get; set; }
        public DatePicker datePickerDateWhenToBuyIt { get; set; }

        public Label labelDateWhenWasBoughtName { get; set; }
        public DatePicker datePickerDateWhenWasBought { get; set; }

        public Button buttonSave { get; set; }

        public static bool isEditedItem = false;

        public static bool isNewPhoto = false;

        public Services.PhotoService _photoService;


        public DesiredItemEditViewModel(int _idDesiredItem)
        {
            createItem();
            setItem(_idDesiredItem);
        }
        private void createItem()
        {
            imageDesiredItem = new Image();
            buttonChangeImage = new Button();

            labelDescriptionName = new Label();
            editorDescription = new Editor();

            entryTitle = new Entry();
            labelTitlenName = new Label();

            labelPriceName = new Label();
            entryPrice = new Entry();

            labelAmountName = new Label();
            labelAmount = new Label();

            labelCategoryName = new Label();
            pickerCategory = new Picker();

            labelBoughtName = new Label();
            switchBought = new Switch();

            labelDateWhenToBuyItName = new Label();
            datePickerDateWhenToBuyIt = new DatePicker();

            labelDateWhenWasBoughtName = new Label();
            datePickerDateWhenWasBought = new DatePicker();

            buttonSave = new Button();
        }
        private void setItem(int _idDesiredItem)
        {
            var _desiredItem = App.dbDesiredItem.getDesiredItemLong(_idDesiredItem);
            _photoService = new Services.PhotoService();


            imageDesiredItem.HeightRequest = 170;
            imageDesiredItem.WidthRequest = 170;

            buttonChangeImage.Text = "Zmień zdjęcie";
            
            
            labelDescriptionName.Text = "Opis: ";
            labelDescriptionName.HorizontalTextAlignment = TextAlignment.Center;
            labelDescriptionName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            editorDescription.HorizontalOptions = LayoutOptions.FillAndExpand;
            editorDescription.VerticalOptions = LayoutOptions.FillAndExpand;
            editorDescription.WidthRequest = 150;

            labelPriceName.Text = "Cena: ";
            labelPriceName.HorizontalTextAlignment = TextAlignment.Center;
            labelPriceName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            entryPrice.HorizontalOptions = LayoutOptions.CenterAndExpand;

            labelAmountName.Text = "Uzbierano: ";
            labelAmountName.HorizontalTextAlignment = TextAlignment.Center;
            labelAmountName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            labelAmount.HorizontalTextAlignment = TextAlignment.Center;
            labelAmount.HorizontalOptions = LayoutOptions.CenterAndExpand;

            labelTitlenName.Text = "Nazwa: ";
            labelTitlenName.HorizontalTextAlignment = TextAlignment.Center;
            labelTitlenName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            entryPrice.HorizontalOptions = LayoutOptions.CenterAndExpand;

            labelCategoryName.Text = "Kategoria:(" + _desiredItem.category + ") ";
            labelCategoryName.HorizontalTextAlignment = TextAlignment.Center;
            labelCategoryName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            pickerCategory.HorizontalOptions = LayoutOptions.FillAndExpand;

            labelBoughtName.Text = "Kupiony: ";
            labelBoughtName.HorizontalTextAlignment = TextAlignment.Center;
            labelBoughtName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            switchBought.HorizontalOptions = LayoutOptions.CenterAndExpand;

            labelDateWhenToBuyItName.Text = "Dodano do listy planowanych produktów: ";
            labelDateWhenToBuyItName.HorizontalTextAlignment = TextAlignment.Center;
            labelDateWhenToBuyItName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            datePickerDateWhenToBuyIt.HorizontalOptions = LayoutOptions.CenterAndExpand;

            labelDateWhenWasBoughtName.Text = "Data zakupu: ";
            labelDateWhenWasBoughtName.HorizontalTextAlignment = TextAlignment.Center;
            labelDateWhenWasBoughtName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            datePickerDateWhenWasBought.HorizontalOptions = LayoutOptions.CenterAndExpand;

            //pickerCategory.Title = _desiredItem.category;
            foreach (var item in App.dbCategory.getAllCategory())
            {
                pickerCategory.Items.Add(item.category);
            }


            imagePathToDelete = _desiredItem.imagePath;
            imageDesiredItem.Source = _desiredItem.imagePath;
            editorDescription.Text = _desiredItem.description;
            entryPrice.Text = _desiredItem.price;
            labelAmount.Text = _desiredItem.amountCollected;
            entryTitle.Text = _desiredItem.title;
            
            if(_desiredItem.bought == "Tak")
            {
                switchBought.IsToggled = true;

            }
            else
            {
                switchBought.IsToggled = false;
            }
            
            datePickerDateWhenToBuyIt.Date = _desiredItem.dateWhenToBuyIt.Date;

            if (_desiredItem.bought == "Tak")
            {
                datePickerDateWhenWasBought.Date = _desiredItem.dateWhenWasBought.Date;
            }
            else
            {
                datePickerDateWhenWasBought.IsVisible = false;
                labelDateWhenWasBoughtName.IsVisible = false;
            }

            switchBought.Toggled += (sender, e) =>
            {
                if(e.Value == true)
                {
                    datePickerDateWhenWasBought.IsVisible = true;
                    datePickerDateWhenWasBought.Date = DateTime.Now.Date;
                    labelDateWhenWasBoughtName.IsVisible = true;
                }
                else
                {
                    datePickerDateWhenWasBought.IsVisible = false;
                    labelDateWhenWasBoughtName.IsVisible = false;
                }
            };


            buttonSave.Text = "Zapisz";


            setButtonClicked(_desiredItem, _idDesiredItem);
        }

        private void setButtonClicked(Model.DesiredItemModelLong _desiredItem, int _idDesiredItem)
        {
            buttonChangeImage.Clicked += async (sender, e) =>
            {

                var answer = await ((NavigationPage)App.Current.MainPage).RootPage.DisplayAlert("Dziś uzbieram na...", "Czy chcesz wykonać nowe zdjęcie?", "Tak", "Nie");
                if (answer == true)
                {

                    await _photoService.takePhoto();

                    if (_photoService.isPhotoExist() == true)
                    {
                        isNewPhoto = true;

                        imageDesiredItem.Source = _photoService.getImageString();
                    }
                }

            };

            buttonSave.Clicked += (sender, e) =>
            {
                var _localDesiredItem = new Model.DesiredItemModelLong();


                if (_photoService.isPhotoExist() == true)
                {
                    _localDesiredItem.imagePath = _photoService.getImageString();
                }
                else
                {
                    _localDesiredItem.imagePath = _desiredItem.imagePath;
                }

                _localDesiredItem.description = editorDescription.Text;
                _localDesiredItem.price = entryPrice.Text;
                _localDesiredItem.amountCollected = labelAmount.Text;
                _localDesiredItem.title = entryTitle.Text;

                if (pickerCategory.SelectedIndex == -1)
                {
                    _localDesiredItem.category = pickerCategory.Title;
                }
                else
                {
                    _localDesiredItem.category = pickerCategory.Items[pickerCategory.SelectedIndex];
                }

                _localDesiredItem.dateWhenToBuyIt = DateTime.SpecifyKind(datePickerDateWhenToBuyIt.Date, DateTimeKind.Local);

                if (switchBought.IsToggled == true)
                {
                    _localDesiredItem.bought = "Tak";
                    _localDesiredItem.dateWhenWasBought = DateTime.SpecifyKind(datePickerDateWhenWasBought.Date, DateTimeKind.Local);
                }
                else
                {
                    _localDesiredItem.bought = "Nie";
                }



                bool _status = App.dbDesiredItem.updateDesiredItem(_localDesiredItem, _idDesiredItem);
                if (_status == true)
                {
                    if (_photoService.isPhotoExist() == true)
                    {
                        if (File.Exists(imagePathToDelete) == true && isNewPhoto == true)
                        {
                            File.Delete(imagePathToDelete);
                            imagePathToDelete = "";
                        }

                        _photoService.deletePhoto();
                    }

                    if (((NavigationPage)App.Current.MainPage).RootPage.GetType().Name == nameof(DzisUzbieramNa.View.DesiredItemsView))
                    {
                        ((View.DesiredItemsView)((NavigationPage)App.Current.MainPage).RootPage)._listView.ItemsSource = App.dbDesiredItem.getAllDesiredItemShort();
                    }


                    isEditedItem = true;
                    isNewPhoto = false;



                    App.Current.MainPage = new NavigationPage(new View.DesiredItemsView());
                    App.Current.MainPage.Navigation.PushAsync(new View.DesiredItemView(_idDesiredItem));
                }

            };
        }
    }
}
