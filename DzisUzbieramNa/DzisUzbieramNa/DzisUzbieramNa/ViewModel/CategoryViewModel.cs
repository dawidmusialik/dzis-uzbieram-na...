﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.ViewModel
{
    public class CategoryViewModel
    {
        public Label labelCategory { get; set; }
        public Image imageDelete { get; set; }
        public Image imageEdit { get; set; }

        public CategoryViewModel()
        {
            createItem();
            setItem();
        }
        private void createItem()
        {
            labelCategory = new Label();
            imageDelete = new Image();
            imageEdit = new Image();
        }
        private void setItem()
        {
            labelCategory.HorizontalOptions = LayoutOptions.CenterAndExpand;
            labelCategory.HorizontalTextAlignment = TextAlignment.Center;
            labelCategory.VerticalTextAlignment = TextAlignment.Center;
            labelCategory.HeightRequest = 50;

            imageDelete.Source = "iconDelete.png";
            imageDelete.HeightRequest = 35;
            imageDelete.WidthRequest = 35;
            imageDelete.HorizontalOptions = LayoutOptions.EndAndExpand;
            imageDelete.VerticalOptions = LayoutOptions.Center;
            imageDelete.IsVisible = false;

            imageEdit.Source = "iconDelete.png";
            imageEdit.HeightRequest = 35;
            imageEdit.WidthRequest = 35;
            imageEdit.HorizontalOptions = LayoutOptions.EndAndExpand;
            imageEdit.VerticalOptions = LayoutOptions.Center;
            imageEdit.IsVisible = false;
        }
    }

    public class CategoryViewCell:ViewCell
    {
        public CategoryViewModel _viewElements;
        StackLayout _stacklayout;
        public CategoryViewCell()
        {
            createItem();
            setItem();
        }

        private void createItem()
        {
            _viewElements = new CategoryViewModel();
            _stacklayout = new StackLayout();
        }

        private void setItem()
        {
            _viewElements.labelCategory.SetBinding(Label.TextProperty, new Binding(path: "category"));

            _stacklayout.Margin = new Thickness(5, 2, 5, 2);
            _stacklayout.Orientation = StackOrientation.Horizontal;
            _stacklayout.Children.Add(_viewElements.labelCategory);
            this.View = _stacklayout;
        }
    }


}
