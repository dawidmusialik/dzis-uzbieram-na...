﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.ViewModel
{
    public class DesiredItemAddViewModel
    {
        public Image imageDesiredItemImage { get; set; }

        public Label labelDesiredItemPrice { get; set; }
        public Entry entryDesiredItemPrice { get; set; }

        public Label labelDesiredItemTitle { get; set; }
        public Entry entryDesiredItemTitle { get; set; }

        public Label labelDesiredItemDescription { get; set; }
        public Editor editorDesiredItemDescription { get; set; }

        public Label labelDesiredItemCategory { get; set; }
        public Picker pickerDesiredItemCategory { get; set; }

        public Button buttonSaveDesiredItem { get; set; }
        public Button buttonTakePhoto { get; set; }

        public Services.PhotoService _photoService { get; set; }

        public static bool idNewItemOrUpdatedItem = false;

        public DesiredItemAddViewModel()
        {

            createItem();
            setItem();

        }

        private void createItem()
        {
            imageDesiredItemImage = new Image();
            _photoService = new Services.PhotoService();

            entryDesiredItemTitle = new Entry();
            labelDesiredItemTitle = new Label();

            labelDesiredItemPrice = new Label();
            entryDesiredItemPrice = new Entry();

            labelDesiredItemDescription = new Label();
            editorDesiredItemDescription = new Editor();

            labelDesiredItemCategory = new Label();
            pickerDesiredItemCategory = new Picker();

            buttonSaveDesiredItem = new Button();
            buttonTakePhoto = new Button();

        }

        private void setItem()
        {
            imageDesiredItemImage.Source = "iconImage.png";
            imageDesiredItemImage.HeightRequest = 150;
            imageDesiredItemImage.WidthRequest = 150;

            labelDesiredItemPrice.Text = "Cena: ";
            entryDesiredItemPrice.Placeholder = "Wprowadź cenę";
            entryDesiredItemPrice.HorizontalOptions = LayoutOptions.FillAndExpand;

            labelDesiredItemTitle.Text = "Nazwa: ";
            entryDesiredItemTitle.Placeholder = "Wprowdź nazwę";
            entryDesiredItemTitle.HorizontalOptions = LayoutOptions.FillAndExpand;

            labelDesiredItemDescription.Text = "Opis: ";
            labelDesiredItemDescription.HorizontalTextAlignment = TextAlignment.Center;
            editorDesiredItemDescription.HorizontalOptions = LayoutOptions.FillAndExpand;
            editorDesiredItemDescription.VerticalOptions = LayoutOptions.FillAndExpand;
            editorDesiredItemDescription.HeightRequest = 100;

            labelDesiredItemCategory.Text = "Kategoria: ";
            pickerDesiredItemCategory.HorizontalOptions = LayoutOptions.FillAndExpand;

            pickerDesiredItemCategory.Title = "Wybierz kategorię";
            foreach (var item in App.dbCategory.getAllCategory())
            {
                pickerDesiredItemCategory.Items.Add(item.category);
            }
            
            buttonSaveDesiredItem.Text = "Zapisz";
            buttonTakePhoto.Text = "Zrób zdjęcie";

            setItem_ButtonClicked();
        }
        private void setItem_ButtonClicked()
        {
            buttonSaveDesiredItem.Clicked += (sender, e) =>
            {
                if (isFieldsEmpty() == false)
                {
                    var _localDesiredItem = new DataBase.Table.DesiredItemTable();
                    _localDesiredItem.amountCollected = 0;
                    _localDesiredItem.bought = false;
                    _localDesiredItem.dateWhenToBuyIt = DateTime.Now;
                    //_localDesiredItem.dateWhenWasBought = null;
                    _localDesiredItem.description = editorDesiredItemDescription.Text;
                    _localDesiredItem.idCategory = App.dbCategory.getIDategory(pickerDesiredItemCategory.Items[pickerDesiredItemCategory.SelectedIndex]);
                    if(_photoService.isPhotoExist() == true)
                    {
                        _localDesiredItem.imagePath = _photoService.getImageString();
                    }
                    else
                    {
                        _localDesiredItem.imagePath = "iconNoImage.png";
                    }
                    _localDesiredItem.price = Convert.ToDouble(entryDesiredItemPrice.Text);
                    _localDesiredItem.title = entryDesiredItemTitle.Text;

                    if(App.dbDesiredItem.addDesiredItem(_localDesiredItem) == true)
                    {
                        _photoService.deletePhoto();
                        imageDesiredItemImage.Source = "iconImage.png";
                        imageDesiredItemImage.HeightRequest = 150;
                        imageDesiredItemImage.WidthRequest = 150;
                        buttonTakePhoto.IsVisible = true;
                        entryDesiredItemPrice.Text = null;
                        entryDesiredItemPrice.Placeholder = "Wprowadź cenę";
                        entryDesiredItemTitle.Text = null;
                        entryDesiredItemTitle.Placeholder = "Wprowdź nazwę";
                        editorDesiredItemDescription.Text = null;
                        pickerDesiredItemCategory.SelectedIndex = -1;

                        idNewItemOrUpdatedItem = true;

                        //if (((NavigationPage)App.Current.MainPage).RootPage.GetType().Name == nameof(DzisUzbieramNa.View.DesiredItemsView))
                        //{
                        //    ((View.DesiredItemsView)((NavigationPage)App.Current.MainPage).RootPage)._listView.ItemsSource = App.dbDesiredItem.getAllDesiredItemShort();
                        //}
                    }
                    else
                    {
                        _photoService.deletePhoto_AppPath();
                        _photoService.deletePhoto_LocalPath();
                        _photoService.deletePhoto();
                        imageDesiredItemImage.Source = "iconImage.png";
                    }

                }
                else
                {
                    App.LocalNotification("Uzupełnij pola");
                }

            };

            buttonTakePhoto.Clicked += async (sender, e) =>
            {
                await _photoService.takePhoto();
                if(_photoService.isPhotoExist() == true)
                {
                    imageDesiredItemImage.Source = _photoService.getImageString();
                    imageDesiredItemImage.HeightRequest = 300;
                    imageDesiredItemImage.WidthRequest = 300;
                    buttonTakePhoto.IsVisible = false;

                    TapGestureRecognizer imageTap = new TapGestureRecognizer();
                    imageTap.Tapped += async (senderImage, eImage) =>
                    {
                       var _answer = await App.Current.MainPage.DisplayAlert("Dziś uzbieram na...", "Czy chcesz usunąć to zdjęcie?", "Tak", "Nie");
                        if(_answer == true)
                        {
                            imageDesiredItemImage.Source = "iconImage.png";
                            imageDesiredItemImage.HeightRequest = 150;
                            imageDesiredItemImage.WidthRequest = 150;
                            buttonTakePhoto.IsVisible = true;
                            _photoService.deletePhoto_AppPath();
                            _photoService.deletePhoto_LocalPath();
                            _photoService.deletePhoto();
                            imageDesiredItemImage.GestureRecognizers.Clear();
                        }
                    };
                    imageDesiredItemImage.GestureRecognizers.Add(imageTap);
                }
            };
        }

        private bool isFieldsEmpty()
        {
            bool _status = false;
            if(entryDesiredItemPrice.Text == null || entryDesiredItemPrice.Text == "")
            {
                _status = true;
            }
            if(entryDesiredItemTitle.Text == null || entryDesiredItemPrice.Text == "")
            {
                _status = true;
            }
            if(editorDesiredItemDescription.Text == null || editorDesiredItemDescription.Text == "")
            {
                _status = true;
            }
            //if(_photoService.isPhotoExist() == false)
            //{
            //    _status = true;
            //}
            if(pickerDesiredItemCategory.SelectedIndex == -1)
            {
                _status = true;
            }

            return _status;
        }

    }
}
