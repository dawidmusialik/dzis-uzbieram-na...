﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.ViewModel
{
    public class AmountCollectedViewModel
    {
        public Label labelAmountDateName { get; set; }
        public Label labelAmountDate { get; set; }

        public Label labelAmountName { get; set; }
        public Label labelAmount { get; set; }


        public AmountCollectedViewModel()
        {
            createItem();
            setItem();
        }

        private void createItem()
        {
            labelAmountDateName = new Label();
            labelAmountDate = new Label();
            labelAmountName = new Label();
            labelAmount = new Label();
        }

        private void setItem()
        {
            labelAmountDateName.Text = "Data: ";
            labelAmountName.Text = "Oszczędności: ";

        }


    }

    public class AmountCollectedViewCell:ViewCell
    {
        StackLayout _stacLayout;
        AmountCollectedViewModel _viewElements;

        public AmountCollectedViewCell()
        {
            createElements();
            setElements();
        }

        private void createElements()
        {
            _stacLayout = new StackLayout();
            _viewElements = new AmountCollectedViewModel();
        }

        private void setElements()
        {
            _viewElements.labelAmount.SetBinding(Label.TextProperty, new Binding(path: "amount"));
            _viewElements.labelAmountDate.SetBinding(Label.TextProperty, new Binding(path: "date"));



            _stacLayout.Orientation = StackOrientation.Horizontal;
            _stacLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Children =
                {
                    _viewElements.labelAmountDateName,
                    _viewElements.labelAmountDate,
                }
            });
            _stacLayout.Children.Add(new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Children =
                {
                    _viewElements.labelAmountName,
                    _viewElements.labelAmount,
                }
            });

            this.View = _stacLayout;
        }

    }

    public class AmountCollectedViewModel_ControlPanel
    {
        public Label labelValue { get; set; }
        public Entry entryValue { get; set; }
        public Button buttonAddition { get; set; }
        public Button buttonSubtraction { get; set; }

        public static bool isNowItemAmountCollected = false;

        public AmountCollectedViewModel_ControlPanel(int _idDesiredItem)
        {
            createItem();
            setItem(_idDesiredItem);
        }

        private void createItem()
        {
            labelValue = new Label();
            entryValue = new Entry();
            buttonAddition = new Button();
            buttonSubtraction = new Button();
        }

        private void setItem(int _idDesiredItem)
        {
            labelValue.Text = "Wartość: ";
            labelValue.FontSize = 16;
            labelValue.VerticalTextAlignment = TextAlignment.Center;
            labelValue.HorizontalTextAlignment = TextAlignment.End;

            entryValue.Placeholder = "Wprowadź wartość";
            entryValue.FontSize = 16;
            entryValue.HorizontalTextAlignment = TextAlignment.Center;
            entryValue.HorizontalOptions = LayoutOptions.CenterAndExpand;

            buttonAddition.Text = "+";
            buttonAddition.FontSize = 16;
            buttonAddition.HorizontalOptions = LayoutOptions.EndAndExpand;
            buttonAddition.WidthRequest = 40;
            buttonSubtraction.Text = "-";
            buttonSubtraction.FontSize = 16;
            buttonSubtraction.HorizontalOptions = LayoutOptions.End;
            buttonSubtraction.WidthRequest = 40;

            setButtonClicked(_idDesiredItem);
        }

        private void setButtonClicked(int _idDesiredItem)
        {
            buttonAddition.Clicked += (sender, e) =>
            {
                if (entryValue.Text != null && entryValue.Text != "")
                {
                    bool status = false;
                    try
                    {
                        double _localDouble = Convert.ToDouble(entryValue.Text);
                        status = true;
                    }
                    catch
                    {
                        status = false;
                        App.LocalNotification("Wprowadzona wartość nie jest liczbowa");
                    }
                    finally
                    {
                        if (status == true)
                        {
                            bool _operationStatus = App.dbAmountCollected.addAmountCollected_Addition(Convert.ToDouble(entryValue.Text), DateTime.Now.Date, _idDesiredItem);

                            if (_operationStatus = true)
                            {
                                viewRefresh(_idDesiredItem);

                                isNowItemAmountCollected = true;

                                entryValue.Text = null;
                                entryValue.Placeholder = "Wprowadź wartość";
                            }
                        }
                    }
                }
            };

            buttonSubtraction.Clicked += (sender, e) =>
            {
                if (entryValue.Text != null && entryValue.Text != "")
                {
                    bool status = false;
                    try
                    {
                        double _localDouble = Convert.ToDouble(entryValue.Text);
                        status = true;
                    }
                    catch
                    {
                        status = false;
                        App.LocalNotification("Wprowadzona wartość nie jest liczbowa");
                    }
                    finally
                    {
                        if (status == true)
                        {
                            bool _operationStatus = App.dbAmountCollected.addAmountCollected_Subtraction(Convert.ToDouble(entryValue.Text), DateTime.Now.Date, _idDesiredItem);

                            if (_operationStatus = true)
                            {
                                viewRefresh(_idDesiredItem);

                                isNowItemAmountCollected = true;
                                entryValue.Text = null;
                                entryValue.Placeholder = "Wprowadź wartość";
                            }
                        }
                    }
                }
            };
        }

        private void viewRefresh(int _idDesiredItem)
        {
            if (((NavigationPage)App.Current.MainPage).CurrentPage.GetType().Name == nameof(DzisUzbieramNa.View.DesiredItemView))
            {
                ((View.DesiredItemView)((NavigationPage)App.Current.MainPage).CurrentPage)._listViewAmount.ItemsSource = App.dbAmountCollected.getAllAmountCollected(_idDesiredItem);
                ((View.DesiredItemView)((NavigationPage)App.Current.MainPage).CurrentPage)._viewElements.labelAmount.Text = Convert.ToString(App.dbDesiredItem.getDesiredItemAmountCollected(_idDesiredItem));
            }
        }

    }


}
