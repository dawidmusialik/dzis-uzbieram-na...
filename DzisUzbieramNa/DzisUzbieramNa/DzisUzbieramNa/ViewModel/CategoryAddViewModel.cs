﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.ViewModel
{
    public class CategoryAddViewModel
    {
        public Label labelName { get; set; }
        public Entry entryCategory { get; set; }
        public Button buttonSaveCategory { get; set; }
        public static bool isNewElements = false;
        public CategoryAddViewModel()
        {
            createItem();
            setItem();
        }

        private void createItem()
        {
            labelName = new Label();
            entryCategory = new Entry();
            buttonSaveCategory = new Button();
        }

        private void setItem()
        {
            labelName.Text = "Nazwa kategorii: ";
            labelName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            labelName.HorizontalTextAlignment = TextAlignment.Center;
            labelName.VerticalTextAlignment = TextAlignment.Center;
            labelName.Margin = new Thickness(0, 10, 0, 5);

            entryCategory.Placeholder = "Wprowadź nazwę";
            entryCategory.HorizontalOptions = LayoutOptions.CenterAndExpand;
            entryCategory.HorizontalTextAlignment = TextAlignment.Center;
            entryCategory.Margin = new Thickness(0, 5, 0, 10);

            buttonSaveCategory.Text = "Zapisz";
            buttonSaveCategory.HorizontalOptions = LayoutOptions.CenterAndExpand;
            buttonSaveCategory.Margin = new Thickness(0, 15, 0, 10);

            setButtonClicked();
        }

        private void setButtonClicked()
        {
            buttonSaveCategory.Clicked += (sender, e) =>
            {
                if (entryCategory.Text != null && entryCategory.Text != "")
                {
                    bool _status = App.dbCategory.addCategory(entryCategory.Text);
                    if (_status == true)
                    {
                        isNewElements = true;
                        entryCategory.Text = null;
                        entryCategory.Placeholder = "Wprowadź nazwę";
                    }
                }
            };
        }

    }
}
