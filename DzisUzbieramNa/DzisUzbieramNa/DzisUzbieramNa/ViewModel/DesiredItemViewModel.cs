﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.ViewModel
{
    public class DesiredItemViewModel
    {
        public Image imageDesiredItem { get; set; }

        public Label labelTitlenName { get; set; }
        public Label labelTitle { get; set; }

        public Label labelDescriptionName { get; set; }
        public Label labelDescription { get; set; }

        public Label labelPriceName { get; set; }
        public Label labelPrice { get; set; }

        public Label labelAmountName { get; set; }
        public Label labelAmount { get; set; }

        public Label labelCategoryName { get; set; }
        public Label labelCategory { get; set; }

        public Label labelBoughtName { get; set; }
        public Label labelBought { get; set; }

        public Label labelDateWhenToBuyItName { get; set; }
        public Label labelDateWhenToBuyIt { get; set; }

        public Label labelDateWhenWasBoughtName { get; set; }
        public Label labelDateWhenWasBought { get; set; }

        public DesiredItemViewModel(int _idDesiredItem)
        {
            createItem();
            setItem(_idDesiredItem);
        }
        
        private void createItem()
        {
            imageDesiredItem = new Image();

            labelDescriptionName = new Label();
            labelDescription = new Label();

            labelTitle = new Label();
            labelTitlenName = new Label();

            labelPriceName = new Label();
            labelPrice = new Label();

            labelAmountName = new Label();
            labelAmount = new Label();

            labelCategoryName = new Label();
            labelCategory = new Label();
            
            labelBoughtName = new Label();
            labelBought = new Label();

            labelDateWhenToBuyItName = new Label();
            labelDateWhenToBuyIt = new Label();

            labelDateWhenWasBoughtName = new Label();
            labelDateWhenWasBought = new Label();
        }
        private void setItem(int _idDesiredItem)
        {
            imageDesiredItem.HeightRequest = 170;
            imageDesiredItem.WidthRequest = 170;

            labelDescriptionName.Text = "Opis: ";
            labelDescriptionName.HorizontalTextAlignment = TextAlignment.Center;
            labelDescription.HorizontalTextAlignment = TextAlignment.Center;
            labelDescription.HeightRequest = 150;

            labelPriceName.Text = "Cena: ";

            labelAmountName.Text = "Uzbierano: ";

            labelTitlenName.Text = "Nazwa: ";
            labelTitlenName.FontAttributes = FontAttributes.Bold;
            labelTitlenName.FontSize = 18;
            labelTitle.FontAttributes = FontAttributes.Bold;
            labelTitle.FontSize = 18;

            labelCategoryName.Text = "Kategoria: ";
            labelCategoryName.FontAttributes = FontAttributes.Bold;
            labelCategoryName.FontSize = 16;
            labelCategory.FontAttributes = FontAttributes.Bold;
            labelCategory.FontSize = 16;

            labelBoughtName.Text = "Kupiony: ";

            labelDateWhenToBuyItName.Text = "Dodano do listy planowanych produktów: ";
            labelDateWhenToBuyItName.HorizontalTextAlignment = TextAlignment.Center;
            //labelDateWhenWasBoughtName.VerticalOptions = LayoutOptions.CenterAndExpand;
            labelDateWhenToBuyIt.HorizontalTextAlignment = TextAlignment.Center;
            //labelDateWhenToBuyIt.VerticalOptions = LayoutOptions.CenterAndExpand;

            labelDateWhenWasBoughtName.Text = "Data zakupu: ";
            labelDateWhenWasBoughtName.HorizontalTextAlignment = TextAlignment.Center;
            //labelDateWhenWasBoughtName.VerticalOptions = LayoutOptions.CenterAndExpand;
            labelDateWhenWasBought.HorizontalTextAlignment = TextAlignment.Center;
            //labelDateWhenWasBought.VerticalOptions = LayoutOptions.CenterAndExpand;


            var _desiredItem = App.dbDesiredItem.getDesiredItemLong(_idDesiredItem);
            imageDesiredItem.Source = _desiredItem.imagePath;
            labelDescription.Text = _desiredItem.description;
            labelPrice.Text = _desiredItem.price;
            labelAmount.Text = _desiredItem.amountCollected;
            labelTitle.Text = _desiredItem.title;
            labelCategory.Text = _desiredItem.category;
            labelBought.Text = _desiredItem.bought;
            labelDateWhenToBuyIt.Text = _desiredItem.dateWhenToBuyIt.Date.ToShortDateString();
            if(_desiredItem.bought == "Tak")
            {
                labelDateWhenWasBought.Text = _desiredItem.dateWhenWasBought.Date.ToShortDateString();
            }
            else
            {
                labelDateWhenWasBought.Text = "Jeszcze nie kupiono";
            }

        }
    }
}
