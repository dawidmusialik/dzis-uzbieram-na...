﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DzisUzbieramNa.Model
{
   public class DesiredItemModelShort
   {
        public int id { get; set; }
        public string imagePath { get; set; }
        public string description { get; set; }
        public string title { get; set; }
        public string price { get; set; }
        public string amountCollected { get; set; }
        public string category { get; set; }
        public Color isBought { get; set; }
   }

    public class DesiredItemModelLong : DesiredItemModelShort
    {
        public string bought { get; set; }
        public DateTime dateWhenToBuyIt { get; set; }
        public DateTime dateWhenWasBought { get; set; }
    }

}
