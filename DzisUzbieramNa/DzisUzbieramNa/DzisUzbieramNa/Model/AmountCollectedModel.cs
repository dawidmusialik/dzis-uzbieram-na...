﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DzisUzbieramNa.Model
{
    public class AmountCollectedModel
    {
        public int id { get; set; }
        public int idItem { get; set; }
        public string amount { get; set; }
        public string date { get; set; }
    }
}
