﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DzisUzbieramNa.Interface
{
    public interface IPhoto
    {
        Task takePhoto();
        void deletePhoto();
        bool deletePhoto_LocalPath();
        bool deletePhoto_AppPath();
        void disposePhoto();
        string getImageString();
        Image getImage();
        bool isPhotoExist();
    }
}
